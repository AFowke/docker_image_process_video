FROM python:3.7-slim

RUN pip3 install awscli

RUN pip3 install boto3

RUN pip3 install --upgrade google-api-python-client

RUN pip3 install google-auth

RUN pip3 install google-auth-httplib2

RUN pip3 install google-auth-oauthlib

RUN pip3 install httplib2

RUN pip3 install requests

RUN pip3 install PyMySQL

RUN apt-get clean

WORKDIR /tmp

COPY credentials.json /tmp

COPY token.pickle /tmp

COPY main.py /tmp

COPY ffmpeg /tmp

COPY ffprobe /tmp

EXPOSE 80

CMD ["python", "main.py"]
