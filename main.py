from __future__ import print_function
import boto3
import os
import subprocess
import json
from datetime import datetime, timedelta
from configparser import ConfigParser
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import googleapiclient.http
import pprint
import pymysql
import requests


host = "playerstation.cetqebkshgfa.ap-southeast-2.rds.amazonaws.com"
port = 3306
dbname = "PlayerStation"
user = "PlayerStation"
password = "Shevchenko1"


# Change path names
# Change back to env var
# Need to install configparser?


access_key = os.environ['access_key']
secret_access_key = os.environ['secret_access_key']
payload = os.environ['payload']



#access_key = 'AKIAIWXZBXDUVUVL6Z5Q'
#secret_access_key = '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'
#payload = '{"Fixture Details":{"Fixture": "53073","Venue": "4","Resource": "9","HomeTeam": "4311","AwayTeam": "1285","HomeTeamName": "Unicorn","AwayTeamName": "Cellphone","HomeTeamScore": "28","AwayTeamScore": "7"}, "Raw Data": [{"EventID": "8325108a-7b40-45eb-bbe2-22f283cb4586", "GameID": 53073, "TeamID": 0, "PlayerID": 0, "DateTime": "2019-05-21 08:15:21.470014", "HomeScore": 0, "AwayScore": 0, "Minutes": "null", "Seconds": "null", "Type": "Session Start", "TeamName": "null"}, {"EventID": "85571c6e-2c74-4ba8-b722-1e6dd1098788", "GameID": 53073, "TeamID": 0, "PlayerID": 0, "DateTime": "2019-05-21 08:16:51.338123", "HomeScore": 0, "AwayScore": 0, "Minutes": "0", "Seconds": "0", "Type": "Timer Start", "TeamName": "null"}, {"EventID": "affd28c2-ca4b-4a7b-a650-8f44a16afa8d", "GameID": 53073, "TeamID": 4311, "PlayerID": 0, "DateTime": "2019-05-21 08:17:58.272256", "HomeScore": 1, "AwayScore": 0, "Minutes": "1", "Seconds": "7", "Type": "Goal", "TeamName": "Authentic FC Legends"}, {"EventID": "35343de8-29ca-43c7-b873-bb244844c5f2", "GameID": 53073, "TeamID": 4311, "PlayerID": 0, "DateTime": "2019-05-21 08:22:57.531508", "HomeScore": 2, "AwayScore": 0, "Minutes": "6", "Seconds": "6", "Type": "Goal", "TeamName": "Authentic FC Legends"}, {"EventID": "19b47424-db04-4ef2-b0d7-1e3650f3513f", "GameID": 53073, "TeamID": 0, "PlayerID": 0, "DateTime": "2019-05-21 08:54:42.811207", "HomeScore": 0, "AwayScore": 0, "Minutes": "null", "Seconds": "null", "Type": "Session End", "TeamName": "null"}], "Camera Details": {"Camera S/N": "ACCC8EBA1307"}, "MKV Details": [{"GUID": "38395b81-7410-41b6-8ddb-f2887c59657e", "Camera SN": "ACCC8EBA1307", "StartTime": "2019-05-21 08:12:34", "StopTime": "2019-05-21 08:17:35", "FilePath": "/Shared/axis-ACCC8EBA1307/20190509/15/20190509_150048_A66D_ACCC8EBA1307/20190521_20/20190521_201234_0D2B.mkv"}, {"GUID": "e316bf1f-9b9f-4422-8d74-f08167b37ce3", "Camera SN": "ACCC8EBA1307", "StartTime": "2019-05-21 08:17:35", "StopTime": "2019-05-21 08:22:36", "FilePath": "/Shared/axis-ACCC8EBA1307/20190509/15/20190509_150048_A66D_ACCC8EBA1307/20190521_20/20190521_201734_F13C.mkv"}]}'


payloadjson = json.loads(payload)

absolutepath = '/tmp/'

# If modifying these scopes, delete the file token.pickle.
SCOPES = ['https://www.googleapis.com/auth/drive.file']


s3_client = boto3.client('s3', aws_access_key_id=access_key, aws_secret_access_key=secret_access_key)

creds = None

if os.path.exists('token.pickle'):
    with open('token.pickle', 'rb') as token:
        creds = pickle.load(token)

if not creds or not creds.valid:
    if creds and creds.expired and creds.refresh_token:
        creds.refresh(Request())

    else:
        flow = InstalledAppFlow.from_client_secrets_file(
            'credentials.json', SCOPES)
        creds = flow.run_local_server()

    with open('token.pickle', 'wb') as token:
        pickle.dump(creds, token)

service = build('drive', 'v3', credentials=creds)


def download(bucket, filename):
    s3_client.download_file(bucket, filename, '{}{}'.format(absolutepath, filename))
    print(filename)


def concat(path_txt, path_mkv_1):
    args = ["./ffmpeg", "-f", "concat", "-safe", "0", "-i", path_txt, "-c", "copy", path_mkv_1]
    popen = subprocess.Popen(args, stdout=subprocess.PIPE)
    popen.wait()
    output = popen.stdout.read()
    print(output)


def clip(input, output, clipstart, clipduration):
    args = ["./ffmpeg", "-ss", clipstart, "-i", input, "-to", clipduration, "-c", "copy", output]
    popen = subprocess.Popen(args, stdout=subprocess.PIPE)
    popen.wait()
    output = popen.stdout.read()
    print(output)


def mkvtomp4(input, output):
    args = ["./ffmpeg", "-i", input, "-codec", "copy", output]
    popen = subprocess.Popen(args, stdout=subprocess.PIPE)
    popen.wait()
    output = popen.stdout.read()
    print(output)


def getconfigsettings(input):
    parser = ConfigParser()
    parser.read('{}{}'.format(absolutepath, input))
    prelengthinput = parser.get('Default', 'pregoalduration')
    postlengthinput = parser.get('Default', 'postgoalduration')
    durationinput = int(prelengthinput) + int(postlengthinput)
    introduration = parser.getfloat('Default', 'introduration')
    clipduration = int(durationinput) - 1
    transitionduration = parser.getfloat('Default', 'transitionduration')
    return prelengthinput, postlengthinput, durationinput, introduration, clipduration, transitionduration


def highlight(input, ouput, start, duration):
    cmds = ['./ffmpeg', '-ss', start, '-i', input, '-c', 'copy', '-t', duration, ouput]
    process = subprocess.Popen(cmds)
    process.wait()

def overlay(inputs, filters, output):
    cmds = ['./ffmpeg -y ' + inputs + ' -filter_complex ' + '"' + filters + '"' + ' -vcodec libx264 -map [out] ' + output]
    process = subprocess.Popen(cmds, shell=True)
    process.wait()


def upload(input, key):
    s3_client.upload_file(input, 'gamemp4', '{}'.format(key))


def make_connection():
    return pymysql.connect(host=host, user=user, passwd=password,
        port=port, db=dbname)


def uploaddrive(input, title):
    media_body = googleapiclient.http.MediaFileUpload(
        input,
        resumable=True
    )

    body = {
        'name': title
    }

    new_file = service.files().create(body=body, media_body=media_body).execute()
    pprint.pprint(new_file)

    file_id = new_file['id']

    new_permission = {
        'type': 'anyone',
        'role': 'reader'
    }

    permissions = service.permissions().create(
        fileId=file_id,
        body=new_permission,
        fields='id'
    ).execute()

    pprint.pprint(permissions)

    return file_id


def delete(file):
    os.remove(file)


def main():
    path_txt = '{}{}'.format(absolutepath, 'concat.txt')
    path_mkv_1 = '{}{}'.format(absolutepath, 'version1.mkv')  # non-clipped concatenated mkv
    path_mkv_2 = '{}{}'.format(absolutepath, 'version2.mkv')  # clipped concatenated mkv
    path_mp4_1 = '{}{}'.format(absolutepath, 'version1.mp4')  # clipped concatenated mp4
    path_mp4_2 = '{}{}'.format(absolutepath, 'version2.mp4')  # highlights
    key_game = '{}_game.mp4'.format(payloadjson['Fixture Details']['Fixture'])
    key_highlights = '{}_highlights.mp4'.format(payloadjson['Fixture Details']['Fixture'])
    downloadkey_config = "Config.ini"
    downloadkey_introvideo = "Intro.mp4"
    downloadkey_transitionvideo = "Transition.mp4"

    for snippet in payloadjson['MKV Details']:
        splitfilepath = snippet['FilePath'].split("/")
        filename = splitfilepath[len(splitfilepath) - 1]
        f = open(path_txt, "a+")  # without knowing why, this has to be completed here.
        f.close
        if os.stat(path_txt).st_size == 0:
            f = open(path_txt, "a+")
            f.write('file ' + "'" + '{}{}'.format(absolutepath, filename) + "'")
            f.close
        else:
            f = open(path_txt, "a+")
            f.write("\n" + 'file ' + "'" + '{}{}'.format(absolutepath, filename) + "'")
            f.close
        f = open(path_txt, "a+")  # again, without knowing why, this must be repeated at the end
        f.close

        download('rawmkv', filename)

    concat(path_txt, path_mkv_1)

    for event in payloadjson['Raw Data']:
        if event['Type'] == "Session Start":
            sessionstart = event['DateTime']
        elif event['Type'] == "Session End":
            sessionend = event['DateTime']
        else:
            pass

    videostart = payloadjson['MKV Details'][0]['StartTime']
    sessionstart_timeobject = datetime.strptime(sessionstart, "%Y-%m-%d %H:%M:%S.%f")
    sessionend_timeobject = datetime.strptime(sessionend, "%Y-%m-%d %H:%M:%S.%f")
    videostart_timeobject = datetime.strptime(videostart, "%Y-%m-%d %H:%M:%S")
    clipstart = str(sessionstart_timeobject - videostart_timeobject)
    totalduration = str(sessionend_timeobject - sessionstart_timeobject)

    clip(path_mkv_1, path_mkv_2, clipstart, totalduration)

    mkvtomp4(path_mkv_2, path_mp4_1)

    download('miscellaneousresources', downloadkey_config)
    download('miscellaneousresources', downloadkey_introvideo)
    download('miscellaneousresources', downloadkey_transitionvideo)

    prelengthinput, postlengthinput, durationinput, introduration, clipduration, transitionduration = getconfigsettings(downloadkey_config)

    prehighlight_timeobject = datetime.strptime(prelengthinput, '%S')
    prehighlight_timedeltaobject = timedelta(seconds=prehighlight_timeobject.second)
    highlight_file_list = ['-i', '{}{}'.format(absolutepath, downloadkey_introvideo)]

    goalcount = 0

    for event in payloadjson['Raw Data']:
        if event['Type'] == "Goal":
            highlight_file_1 = '{}{}.mp4'.format(absolutepath, event['EventID'])
            highlight_file_2 = '"{}{}.mp4"'.format(absolutepath, event['EventID'])
            highlight_file_list.extend(['-i',highlight_file_2, '-i', '{}{}'.format(absolutepath, downloadkey_transitionvideo)])
            event_timeobject = datetime.strptime(event['DateTime'], "%Y-%m-%d %H:%M:%S.%f")
            highlight_start = str(event_timeobject - sessionstart_timeobject - prehighlight_timedeltaobject)
            highlight(path_mp4_1, highlight_file_1, highlight_start, str(durationinput))
            goalcount += 1
        else:
            pass

    delay = introduration

    for y in range(goalcount * 2):
        if y == 0:
            filters = "[{}:v]setpts=PTS-STARTPTS/TB[v{}]".format(str(y), str(y))
        elif y == 1:
            filters = "{}; [{}:v]fade=t=in:st=0:d=0.5:alpha=1,setpts=PTS-STARTPTS+{}/TB[v{}]".format(filters, str(y), str(introduration), str(y))
        else:
            if y % 2 == 0:
                delay += clipduration
            else:
                delay += transitionduration
            filters = "{}; [{}:v]fade=t=in:st=0:d=0.5:alpha=1,setpts=PTS-STARTPTS+{}/TB[v{}]".format(filters, str(y), str(delay), str(y))
    for y in range(goalcount * 2 - 1):
        if y == 0:
            filters = "{}; [v{}][v{}]overlay[over{}]".format(filters, str(y), str(y+1), str(y+1))
        elif y == goalcount * 2  - 2:
            filters = "{}; [over{}][v{}]overlay[out]".format(filters, str(y), str(y + 1))
        else:
            filters = "{}; [over{}][v{}]overlay[over{}]".format(filters, str(y), str(y+1), str(y+1))

    inputs = " ".join(highlight_file_list)

    overlay(inputs, filters, path_mp4_2)

    game_file_id = uploaddrive(path_mp4_1, key_game)
    highlights_file_id = uploaddrive(path_mp4_2, key_highlights)

    conn = make_connection()

    try:
        with conn.cursor() as cursor:
            # Create a new record
            sql = "INSERT INTO `Game_Video_Database` (`GameID`, `GameLink`, `HighlightsLink`) VALUES (%s, %s, %s) ON DUPLICATE KEY UPDATE `GameLink`=(%s), `HighlightsLink`=(%s)"
            cursor.execute(sql, (int(payloadjson['Fixture Details']['Fixture']), game_file_id, highlights_file_id, game_file_id, highlights_file_id))
        conn.commit()
    except:
        pass

    conn.close()

    Video_Links = {'HighlightsLink': highlights_file_id, 'GameLink': game_file_id}

    payloadjson['Video Links'] = Video_Links

    payloaddumped = json.dumps(payloadjson)

    # defining a params dict for the parameters to be sent to the API
    PARAMS = {
       "input": '{}'.format(payloaddumped),
       "stateMachineArn": "arn:aws:states:ap-southeast-2:053921395622:stateMachine:Distribution_Machine"
    }

    # api-endpoint
    URL = "https://d5ghoifzxc.execute-api.ap-southeast-2.amazonaws.com/init"

    # sending get request and saving the response as response object
    r = requests.post(url=URL, json=PARAMS)

    #    upload(path_mp4_1, key)

    for file in os.listdir(absolutepath):
        delete(absolutepath + file)


main()

